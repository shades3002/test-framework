<?php

namespace app\controllers;

use Yii;
use app\models\UsersPayments;
use app\models\UsersPaymentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * AssociatesController implements the CRUD actions for UsersPayments model.
 */
class AssociatesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create','update','delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create','update','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * Lists all UsersPayments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersPaymentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UsersPayments model.
     * @param integer $users_idusers
     * @param integer $payments_idpayment
     * @return mixed
     */
    public function actionView($users_idusers, $payments_idpayment)
    {
        return $this->render('view', [
            'model' => $this->findModel($users_idusers, $payments_idpayment),
        ]);
    }

    /**
     * Creates a new UsersPayments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UsersPayments();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'users_idusers' => $model->users_idusers, 'payments_idpayment' => $model->payments_idpayment]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UsersPayments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $users_idusers
     * @param integer $payments_idpayment
     * @return mixed
     */
    public function actionUpdate($users_idusers, $payments_idpayment)
    {
        $model = $this->findModel($users_idusers, $payments_idpayment);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'users_idusers' => $model->users_idusers, 'payments_idpayment' => $model->payments_idpayment]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UsersPayments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $users_idusers
     * @param integer $payments_idpayment
     * @return mixed
     */
    public function actionDelete($users_idusers, $payments_idpayment)
    {
        $this->findModel($users_idusers, $payments_idpayment)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UsersPayments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $users_idusers
     * @param integer $payments_idpayment
     * @return UsersPayments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($users_idusers, $payments_idpayment)
    {
        if (($model = UsersPayments::findOne(['users_idusers' => $users_idusers, 'payments_idpayment' => $payments_idpayment])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
