<?php

namespace app\controllers;

use Yii;
use app\models\Favorites;
use app\models\FavoritesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * FavoritesController implements the CRUD actions for Favorites model.
 */
class FavoritesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create','update','delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create','update','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Favorites models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FavoritesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Favorites model.
     * @param integer $favorite_user_code
     * @param integer $users_idusers
     * @return mixed
     */
    public function actionView($favorite_user_code, $users_idusers)
    {
        return $this->render('view', [
            'model' => $this->findModel($favorite_user_code, $users_idusers),
        ]);
    }

    /**
     * Creates a new Favorites model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Favorites();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'favorite_user_code' => $model->favorite_user_code, 'users_idusers' => $model->users_idusers]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Favorites model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $favorite_user_code
     * @param integer $users_idusers
     * @return mixed
     */
    public function actionUpdate($favorite_user_code, $users_idusers)
    {
        $model = $this->findModel($favorite_user_code, $users_idusers);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'favorite_user_code' => $model->favorite_user_code, 'users_idusers' => $model->users_idusers]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Favorites model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $favorite_user_code
     * @param integer $users_idusers
     * @return mixed
     */
    public function actionDelete($favorite_user_code, $users_idusers)
    {
        $this->findModel($favorite_user_code, $users_idusers)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Favorites model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $favorite_user_code
     * @param integer $users_idusers
     * @return Favorites the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($favorite_user_code, $users_idusers)
    {
        if (($model = Favorites::findOne(['favorite_user_code' => $favorite_user_code, 'users_idusers' => $users_idusers])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
