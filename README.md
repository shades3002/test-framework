# FRAMEWORK TEST
============================

# Features
* PHP
* Mysql

# Motivation

appraise the developer's skills by using a frameword

# Installation

Note: follow the steps to see in operation. You must have php, mysql and composer installed

1. Clone the project 
```
$ git clone git@gitlab.com:shades3002/test-framework.git
```
2. Install: 
```$ cd frameworkPhp
$ composer global require "fxp/composer-asset-plugin:^1.3.1"
$ composer install```
3. Database:
    import the database that is in the path ```frameworkPhp/resources/users_test.sql``` using the phpmyadmin tool
4. Run:
```
$ php -S localhost:8080 -t .
```
5. Open Browser:
[http://localhost:8080](http://localhost:8080)

# Note: ```user: admin & password: admin```

# Contributors
- Carlos Gutierrez