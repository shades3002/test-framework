<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%users}}".
 *
 * @property integer $idusers
 * @property string $user
 * @property string $password
 * @property string $age
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user', 'password', 'age'], 'required'],
            [['user', 'password', 'age'], 'string', 'max' => 45],
            ['age', 'compare', 'compareValue' => 18, 'operator' => '>=', 'type' => 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idusers' => Yii::t('app', 'Idusers'),
            'user' => Yii::t('app', 'User'),
            'password' => Yii::t('app', 'Password'),
            'age' => Yii::t('app', 'Age'),
        ];
    }

    /**
     * @inheritdoc
     * @return UsersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UsersQuery(get_called_class());
    }
}
