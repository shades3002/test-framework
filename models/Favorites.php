<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%favorites}}".
 *
 * @property integer $favorite_user_code
 * @property integer $users_idusers
 */
class Favorites extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%favorites}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['favorite_user_code', 'users_idusers'], 'required'],
            [['favorite_user_code', 'users_idusers'], 'integer'],
            ['users_idusers', 'compare', 'compareAttribute' => 'favorite_user_code', 'operator' => '!==', 'type' => 'number'],
            [['favorite_user_code', 'users_idusers'], 'unique', 'targetAttribute' => ['favorite_user_code', 'users_idusers']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'favorite_user_code' => Yii::t('app', 'Favorite User Code'),
            'users_idusers' => Yii::t('app', 'Users Idusers'),
        ];
    }

    /**
     * @inheritdoc
     * @return FavoritesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FavoritesQuery(get_called_class());
    }
}
