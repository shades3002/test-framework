<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UsersPayments]].
 *
 * @see UsersPayments
 */
class UsersPaymentsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return UsersPayments[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UsersPayments|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
