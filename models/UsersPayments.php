<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%users_payments}}".
 *
 * @property integer $users_idusers
 * @property integer $payments_idpayment
 */
class UsersPayments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users_payments}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['users_idusers', 'payments_idpayment'], 'required'],
            [['users_idusers', 'payments_idpayment'], 'integer'],
            [['users_idusers', 'payments_idpayment'], 'unique', 'targetAttribute' => ['users_idusers', 'payments_idpayment']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'users_idusers' => Yii::t('app', 'Users Idusers'),
            'payments_idpayment' => Yii::t('app', 'Payments Idpayment'),
        ];
    }

    /**
     * @inheritdoc
     * @return UsersPaymentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UsersPaymentsQuery(get_called_class());
    }
}
