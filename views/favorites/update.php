<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Favorites */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Favorites',
]) . $model->favorite_user_code;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Favorites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->favorite_user_code, 'url' => ['view', 'favorite_user_code' => $model->favorite_user_code, 'users_idusers' => $model->users_idusers]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="favorites-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
