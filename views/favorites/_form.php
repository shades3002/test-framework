<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Users;

/* @var $this yii\web\View */
/* @var $model app\models\Favorites */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="favorites-form col-xs-8">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'users_idusers')->dropDownList(
        ArrayHelper::map(Users::find()->all(),'idusers','user'),
            ['prompt'=>'Selecct a user']
    )->label('Id Users') ?> 
    
    <?= $form->field($model, 'favorite_user_code')->dropDownList(
        ArrayHelper::map(Users::find()->all(),'idusers','user'),
            ['prompt'=>'Selecct a Favorite user']
    )->label('Favorite user') ?> 


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
