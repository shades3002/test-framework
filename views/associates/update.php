<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UsersPayments */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Users Payments',
]) . $model->users_idusers;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->users_idusers, 'url' => ['view', 'users_idusers' => $model->users_idusers, 'payments_idpayment' => $model->payments_idpayment]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="users-payments-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
