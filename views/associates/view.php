<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UsersPayments */

$this->title = $model->users_idusers;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-payments-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'users_idusers' => $model->users_idusers, 'payments_idpayment' => $model->payments_idpayment], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'users_idusers' => $model->users_idusers, 'payments_idpayment' => $model->payments_idpayment], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Id User',
                'attribute' => 'users_idusers',
                'value' => $model->users_idusers
            ],
            [
                'label' => 'Id Payment',
                'attribute' => 'payments_idpayment',
                'value' => $model->payments_idpayment
            ],
        ],
    ]) ?>

</div>
