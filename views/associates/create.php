<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UsersPayments */

$this->title = Yii::t('app', 'Create Users Payments');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-payments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
