<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Users;
use app\models\Payments;

/* @var $this yii\web\View */
/* @var $model app\models\UsersPayments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-payments-form col-xs-8">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'users_idusers')->dropDownList(
        ArrayHelper::map(Users::find()->all(),'idusers','user'),
            ['prompt'=>'Selecct a users']
    )->label('Id Users') ?> 

    <?= $form->field($model, 'payments_idpayment')->dropDownList(
        ArrayHelper::map(Payments::find()->all(),'idpayment',
        function($model, $defaultValue) {
            return 'amount: '.$model['amount'].' date: '.$model['date'];
        }),
        ['prompt'=>'Selecct a payments']
    )->label('Id Payment') ?> 

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
